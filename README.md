# image-toy

Literally just me toying around with Haskell to produce images.

Idea "inspired by" (i.e. stolen from) ["Why functional programming still matters"](https://www.youtube.com/watch?v=vGVJYoKIzjU) talk

Note that this is just for toying around. If you are looking for something like this except more
powerful and efficient, check out the [diagrams](https://archives.haskell.org/projects.haskell.org/diagrams/) project.

If you were looking for real-time graphics, check out the [sdl2](http://hackage.haskell.org/package/sdl2)
and [OpenGL](http://hackage.haskell.org/package/sdl2) packages.