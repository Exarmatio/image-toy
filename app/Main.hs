module Main where

import Region
import qualified Rasterize
import qualified Codec.Picture as P
import qualified Codec.Picture.Png as P
import qualified Data.ByteString.Lazy as B
import System.IO

picture :: Region
picture = translate (Vector 0.5 0.5) $
 Region.join [circles 12, rotate (pi/4) . scaleY 1.5 . scale 0.15 $ rectangle]

circles :: Int -> Region
circles n = Region.join $
 map ithCircle [1..n] where
  ithCircle i 
    = rotate (2*pi/ fromIntegral n * fromIntegral i)
    . translate (Vector 0.3 0)
    . scale 0.02
    $ circle

renderPicture :: Int -> Int -> Region -> P.Image P.Pixel8
renderPicture w h r = Rasterize.rasterize w h (scaleX (fromIntegral w) . scaleY (fromIntegral h) $ r)

main :: IO ()
main = withFile "out.png" WriteMode $ \h -> do
  let png = P.encodePng (renderPicture 500 500 picture)
  B.hPutStr h png