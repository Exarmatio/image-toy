module RegionSpec (spec) where

import Test.Hspec hiding (shouldContain)
import Region

shouldContain = flip shouldSatisfy

spec :: Spec
spec = do
  describe "circle" $ do
    it "should contain all points around origin" $ do
        circle `shouldContain` Point 0 0
        circle `shouldContain` Point (-1) 0
        circle `shouldContain` Point 1 0
        circle `shouldContain` Point 0.2 0.2
    it "shouldn't contain any points distant from origin" $ do
        invert circle `shouldContain` Point 1 1
        invert circle `shouldContain` Point (-2) 0
  describe "scale" $ do
    it "should scale things" $ do
        let scaled = scale 100 circle
        scaled `shouldContain` Point 0 0
        scaled `shouldContain` Point (-100) 0
        scaled `shouldContain` Point 100 0
        scaled `shouldContain` Point 20 20
        invert scaled `shouldContain` Point 100 100
        invert scaled `shouldContain` Point (-200) 0
  describe "translate" $ do
    it "should translate things" $ do
        let translated = translate (Vector 100 100) circle
        translated `shouldContain` Point 100 100
        translated `shouldContain` Point 99 100
        translated `shouldContain` Point 101 100
        translated `shouldContain` Point 100.2 100.2
        invert translated `shouldContain` Point 101 101
        invert translated `shouldContain` Point 98 100
  describe "scale and composition together" $ do
    it "should be composable" $ do
        let bigCircle = translate (Vector 400 300) . scale 100 $ circle
        bigCircle `shouldContain` Point 400 300
        bigCircle `shouldContain` Point 300 300
        bigCircle `shouldContain` Point 500 300
        bigCircle `shouldContain` Point 420 320