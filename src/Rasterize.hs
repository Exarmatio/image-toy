module Rasterize where

import qualified Region

import qualified Codec.Picture as P
import qualified Data.Vector.Storable as V

rasterize :: Int -> Int -> Region.Region -> P.Image P.Pixel8
rasterize width height region = P.Image width height pixels where
    pixels = V.generate (width*height) renderPixel
    renderPixel i = let
      x = i `mod` width
      y = i `div` width
      in render (region $ Region.Point (fromIntegral x) (fromIntegral y))
    render True = black
    render False = white 
    black = 0
    white = 255