module Region where

type Length = Double
data Point = Point !Length !Length
    deriving (Show, Read)

data Vector = Vector !Length !Length
    deriving (Show, Read)

type Region = Point -> Bool

rectangle :: Region
rectangle (Point x y) =
    (-1) <= x && x <= 1
    && (-1) <= y && y <= 1

circle :: Region
circle (Point x y) =
    (x*x + y*y) <= 1

invert :: Region -> Region
invert = (not .)

-- | Move region by some offset
translate :: Vector -> Region -> Region
translate (Vector dx dy) r (Point x y) =
    r (Point (x - dx) (y - dy))

scaleX :: Length -> Region -> Region
scaleX s r (Point x y) =
    r (Point (x/s) y)

scaleY :: Length -> Region -> Region
scaleY s r (Point x y) =
    r (Point x (y/s))

scale :: Length -> Region -> Region
scale s = scaleX s . scaleY s

join :: [Region] -> Region
join rs p = any ($p) rs

rotate :: Length -> Region -> Region
rotate angle r (Point x y) =
    r (Point x' y') where
    x' = x*cosa + y*sina
    y' = y*cosa - x*sina
    sina = sin angle
    cosa = cos angle
